class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body
      t.integer :visit_count, default: 0
      t.string :slug

      t.timestamps
    end
  end
end
