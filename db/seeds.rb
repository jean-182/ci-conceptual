20.times do
  post = Post.new(
    title:  Faker::Food.dish,
    body:   Faker::Food.description,
  )
  post.save
end
