require "rails_helper"

RSpec.describe PostsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/posts").to route_to("posts#index")
    end

    it "routes to #new" do
      expect(get: "/posts/new").to route_to("posts#new")
    end

    it "routes to #show" do
      expect(get: "/posts/slug").to route_to("posts#show", slug: "slug")
    end

    it "routes to #edit" do
      expect(get: "/posts/slug/edit").to route_to("posts#edit", slug: "slug")
    end

    it "routes to #create" do
      expect(post: "/posts").to route_to("posts#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/posts/slug").to route_to("posts#update", slug: "slug")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/posts/slug").to route_to("posts#update", slug: "slug")
    end

    it "routes to #destroy" do
      expect(delete: "/posts/slug").to route_to("posts#destroy", slug: "slug")
    end
  end
end
