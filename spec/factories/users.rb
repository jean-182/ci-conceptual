FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    name { Faker::FunnyName.two_word_name }
    password { "123456" }
  end
end
