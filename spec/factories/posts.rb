FactoryBot.define do
  factory :post do
    factory :valid_post do
      title { Faker::Food.dish }
      body { Faker::Food.description }
      visit_count { 0 }
    end

    factory :invalid_post do
      title { nil }
      body { nil }
      visit_count { nil }
    end
  end
end
