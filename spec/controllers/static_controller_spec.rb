require 'rails_helper'

RSpec.describe StaticController, type: :controller do
  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe "GET #about" do
    it "returns a success response" do
      get :about, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe "GET #help" do
    it "returns a success response" do
      get :help, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end
end
