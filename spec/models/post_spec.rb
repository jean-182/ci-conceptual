require 'rails_helper'

RSpec.describe Post, type: :model do
  context 'model validations and associations' do
    it { should validate_presence_of(:title) }
    it { should validate_length_of(:body).is_at_least(5) }
  end

  context 'validation post' do
    it 'is valid with valid attributes' do
      post = create(:valid_post)
      expect(post).to be_valid
    end
  end
end
