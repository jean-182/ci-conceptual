# CI-Conceptual

## Dependencies

- Ruby 2.4.1
- Rails 5.2.1
- PostgreSQL v9.6 or greater

## Instances

Development - [https://ci-conceptual-development.herokuapp.com](https://ci-conceptual-development.herokuapp.com)

    Instance which we can deploy specific brances in order to do some testing before merge to staging server or demo to the client. Only for internal purposes.

Staging - [https://ci-conceptual-staging.herokuapp.com](https://ci-conceptual-staging.herokuapp.com)

    Instance which we deploy new features that are ready for the client to review and approve before pushing to production.

Production - [https://ci-conceptual.herokuapp.com](https://ci-conceptual.herokuapp.com)

    Instance which will have the released and approved features that the customers are going to use.

## Development Setup

### Define ruby gemset

If using RVM, be sure to set up your gemset:

    cd ci-conceptual
    echo "ci-conceptual" > .ruby-gemset
    cd ..
    cd ci-conceptual

### Bundle

    gem install bundler
    bundle

### Create database

    rails db:create

### Migrate and seed database

    rails db:setup
