class Post < ApplicationRecord
  validates :title, presence: true
  validates :body, length: { minimum: 5 }
  validates :slug, uniqueness: true
  before_create :set_slug

  def update_visit_count
    update(visit_count: visit_count + 1)
  end

  def to_param
    slug
  end

  private

  def set_slug
    self.slug = "#{title.parameterize}-#{SecureRandom.hex(4)}"
  end
end
