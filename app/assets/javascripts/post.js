$(document).on("turbolinks:load", function () {
  $(function () {
    $(document.body).off('click', 'ul.pagination a');
    $(document.body).on('click', 'ul.pagination a', function (e) {
      e.preventDefault();
      var url = $(this).attr("href")
      window.history.pushState(url, window.title, url);
    });
  });
});