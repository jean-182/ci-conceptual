Rails.application.routes.draw do
  devise_for :users,
             path:       '',
             path_names: {
               sign_in:      'login',
               sign_out:     'logout',
               password:     'secret',
               confirmation: 'verification',
               unlock:       'unblock',
               registration: 'register',
               sign_up:      'cmon-let-me-in',
             }
  resources :posts, param: :slug
  root 'static#index'
  get 'about', to: 'static#about'
  get 'help', to: 'static#help'
end
